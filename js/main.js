//Code for Hamburger Menu
$(".cross").hide();
$(".menu").hide();
$(".hamburger").click(function () {
  "use strict";
  $(".menu").slideToggle("slow", function () {
    $(".hamburger").hide();
    $(".cross").show();
  });
});